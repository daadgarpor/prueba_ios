//
//  FirstClass.swift
//  prueba
//
//  Created by Daniel Garcia on 11/09/21.
//

import Foundation

class FirstClass: SecondProtocol{
    func call() {
        print("soy el protocolo")
    }
    
    func callSecondClass() {
        let second = SecondClass()
        second.delegate = self
        second.callFirst()
    }
}
