//
//  protocol.swift
//  prueba
//
//  Created by Daniel Garcia on 11/09/21.
//

import Foundation

protocol PersonProtocol {
    var name: String { get set }
    var age: Int { get set }
    
    func fullName() -> String
}


protocol SecondProtocol {
    func call()
}

struct Programer: PersonProtocol {
    func fullName() -> String {
        return "El nombre es \(name) y age \(age) y programo \(lenguaje)"
    }
    
    var name: String
    
    var age: Int
    
    var lenguaje: String
    
}

struct Teacher: PersonProtocol {
    func fullName() -> String {
        return "El nombre es \(name) y age \(age) y programo \(clase)"
    }
    
    var name: String
    
    var age: Int
    
    var clase: String
}

let pro = Programer(name: "daniel", age: 34, lenguaje: "switf")
let profe = Teacher(name: "juanito", age: 45, clase: "mate")
