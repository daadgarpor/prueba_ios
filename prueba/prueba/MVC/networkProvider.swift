//
//  networkProvider.swift
//  prueba
//
//  Created by Daniel Garcia on 15/09/21.
//

import Foundation
import Alamofire

final class Provider{
    static let shared = Provider()
    
    
    private let URL_CONSULTA = "https://us-central1-bibliotecadecontenido.cloudfunctions.net/helloWorld"
 
    func obtenerInfo(success: @escaping (_ question: [Question])->(), failure: @escaping (_ error: Error?)->()){
        
        AF.request(URL_CONSULTA, method: .get).validate().responseDecodable(of: infoResponse.self ){
            response in
            
            if let info = response.value?.questions{
               success(info)

              // print(info)
            }else{
                failure(response.error)
               // print(response.error as Any)
            }
            
            
           
        }
    }
    
    
}


