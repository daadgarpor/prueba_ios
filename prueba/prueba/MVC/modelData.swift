//
//  modelData.swift
//  prueba
//
//  Created by Daniel Garcia on 15/09/21.
//

import Foundation
struct infoResponse: Decodable{
    let colors: [String]?
    let questions: [Question]?
}

struct Question: Decodable {
    let total: Int?
    let text: String?
    let chartData: [ChartDatum]?
}

struct ChartDatum: Decodable {
    let text: String?
    let percetnage: Int?
}
