//
//  ViewController.swift
//  prueba
//
//  Created by Daniel Garcia on 11/09/21.
//

import UIKit

class ViewController: UIViewController {
   
    var tama: Int?
   
    @IBOutlet weak var tableView: UITableView!
    private var preguntas = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // MARK: - Crear Tabla -
        // MARK: - Crear Celdas -
        // MARK: - Crear ModelData -
        // MARK: - Crear Controller para consumo de servicios -
        // MARK: - Crear Graficos con la respuesta -
        // MARK: - Crear BD en Firebase
        // MARK: - Subir información
     
        consulta()
   
      
        //extenciones
        tableView.dataSource = self
        tableView.delegate = self
            
 
        
        //Registrar celda a utilizar
        tableView.register(UINib(nibName: "nameCell", bundle: nil), forCellReuseIdentifier: "cellName")
        tableView.register(UINib(nibName: "cellFoto", bundle: nil), forCellReuseIdentifier: "cellFoto")
        tableView.register(UINib(nibName: "cellGraficos", bundle: nil), forCellReuseIdentifier: "cellGraficos")
        
     
    }
    
  
  
    
    func consulta() {
        
        Provider.shared.obtenerInfo(){
            (question) in
            question.forEach(){
                ask in
               // print(ask.text as Any)
                self.preguntas.append(ask.text!)
              
            }
            self.tableView.reloadData()

        }failure: {
            (error) in
          
        }
    }
    
}

extension ViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 2 {
            return preguntas.count
        }
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellName", for: indexPath) as? nameCell
        if indexPath.section == 0 {
           
            let nombre =  cell!.name.text
           // print(nombre as Any)
            return cell!
        }
        
        if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellFoto", for: indexPath) as? cellFoto
            
            return cell!
        }
       
        if indexPath.section == 2 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellGraficos", for: indexPath) as? cellGraficos
        
          
         
            
            cell!.pregunta?.text = preguntas[indexPath.row]
          

          
            
           
            
            return cell!
        }
        
        
        
        return cell!
    }
    
    
    
    
}
//deteccion de eventos cumplir protocolo
extension ViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        if indexPath.section == 0 {
            print()
        }
    }
}

