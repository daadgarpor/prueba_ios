//
//  SecondClass.swift
//  prueba
//
//  Created by Daniel Garcia on 11/09/21.
//

import Foundation

class SecondClass {
    
    
    var delegate: SecondProtocol?
    func callFirst() {
        sleep(5)
        
        delegate?.call()
    }
}
